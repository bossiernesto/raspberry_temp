var app = require('http').createServer(handler),
  io = require('socket.io').listen(app),
	fs = require('fs'),
  sys = require('util'),
  exec = require('child_process').exec,
  child;
//Usamos el puerto 8000
app.listen(8000);

function handler(req, res) {
	fs.readFile(__dirname+'/index.html', function(err, data) {
		if (err) {
			console.log(err);
			res.writeHead(500);
			return res.end('Error loading index.html');
		}
		console.log('successfully received a GET request from '+ req);
		res.writeHead(200);
		res.end(data);
	});
}
 
//Cada 5 segundos actualizamos el gráfico con un nuevo valor. 
io.sockets.on('connection', function(socket) {
  setInterval(function(){
    child = exec("cat /sys/class/thermal/thermal_zone0/temp", function (error, stdout, stderr) {
    if (error !== null) {
      console.log('exec error: ' + error);
    } else {
      var date = new Date().getTime();
      var temp = parseFloat(stdout)/1000;
      console.log('Temperature to be emited ' + temp + 'C at time '+ date);
      socket.emit('temperatureUpdate', date, temp); 
    }
  });}, 5000);
});
